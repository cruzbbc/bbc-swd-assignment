describe('bbcController', function() {

    /* App init and config before every test */ 
    var $scope;
    beforeEach(module('bbc'));

    beforeEach(inject(function($rootScope, $controller) {
        $scope = $rootScope.$new();
        $controller('bbcController', {$scope: $scope});
    }));

    
});