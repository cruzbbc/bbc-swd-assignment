app.controller('appController', ['$scope', 'mediaService', function($scope, mediaService) {
    
    $scope.media                = [];
    $scope.isMediaLoading       = true;
    $scope.pageWidth            = 728;
    $scope.nrItemsPerPage       = 5;
    $scope.pageWrapperPosition  = 0;
    $scope.currentPage          = 0;

    /* Gets the data from the service */
    mediaService.getData().then(function(data){
        $scope.isMediaLoading    = false;
        $scope.media             = data;
        $scope.nrPages           = Math.ceil(data.length / $scope.nrItemsPerPage);
    });

    /* Carousel function */
    $scope.moveToPage = function (index) {
    	$scope.currentPage         = index;
		$scope.pageWrapperPosition = -(index * $scope.pageWidth) + 'px';
    };

}]);